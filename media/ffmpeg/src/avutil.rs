// Copyright 2022 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

pub const AV_NOPTS_VALUE: u64 = 0x8000000000000000;

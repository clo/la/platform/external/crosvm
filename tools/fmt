#!/usr/bin/env python3
# Copyright 2022 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# Run `rustfmt` on all Rust code contained in the crosvm workspace, including
# all commmon/* crates as well.
#
# Usage:
#
#    $ bin/fmt
#
# To print a diff and exit 1 if code is not formatted, but without changing any
# files, use:
#
#    $ bin/fmt --check
#

from impl.common import (
    CROSVM_ROOT,
    find_scripts,
    parallel,
    run_main,
    cmd,
    chdir,
    find_source_files,
)
from pathlib import Path

mdformat = cmd("mdformat")
rustfmt = cmd(cmd("rustup which rustfmt"))
black = cmd("black", "--line-length 100")

# How many files to check at once in each thread.
BATCH_SIZE = 8

# Files not under our control or auto-generated.
IGNORE = [
    "docs/book/src/appendix/memory_layout.md",  # mdformat messes up the tables.
    "infra/README.recipes.md",
    "infra/recipes.py",
]


def main(check: bool = False):
    chdir(CROSVM_ROOT)
    check_arg = "--check" if check else None

    print(f"{'Checking format' if check else 'Formatting'}: Rust, Markdown")
    parallel(
        *rustfmt(check_arg).foreach(find_source_files("rs"), batch_size=BATCH_SIZE),
        *mdformat("--wrap 100", check_arg).foreach(
            find_source_files("md", IGNORE),
            batch_size=BATCH_SIZE,
        ),
        *black(check_arg).foreach(
            (
                *find_source_files("py", IGNORE),
                *find_scripts(Path("tools"), "/usr/bin/env python3"),
            ),
            batch_size=BATCH_SIZE,
        ),
    ).fg()


if __name__ == "__main__":
    run_main(main)

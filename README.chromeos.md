# Crosvm on ChromeOS

Crosvm is an independently developed project and is using different workflows than other ChromeOS
repositories.

Please review [Crosvm on Chromeos](https://google.github.io/crosvm/integration/chromeos.html) for
how to submit code and how changes are brought into ChromeOS.
